$(function(){

	checkAll();
	countAll();
	//单击+-符号改变商品数量
	$(".hand").click(function(){
		hand($(this));
	});
	//为全选复选框绑定单击事件
	$("#allCheckBox").click(function(){
		checkAll();
	});
	//当复选框单击时重新计算价格和积分
	$("input[name='cartCheckBox']").click(function(){
		countAll();
	});
	//为删除选中按钮绑定单击事件
	$("#deleteAll").click(function(){
		deleteAll();
	});
	$("td.cart_td_8 a").click(function(){
		$(this).parent().parent().hide().prev().hide();
		$(this).parent().siblings(".cart_td_1").children("input").attr("checked",false);			//删除后设置复选框为未选中,避免计费错误
		countAll();			//删除后重新计算积分和价格
	});
});
//删除选中
function deleteAll()
{
	$("input[name='cartCheckBox']").each(function(){
		if($(this).attr("checked")=="checked")
		{
			$(this).parent().parent().hide().prev().hide();
			$(this).parent().siblings(".cart_td_1").children("input").attr("checked",false);			//删除后设置复选框为未选中,避免计费错误
		}
		countAll();			 //删除后重新计算积分和价格
	});
}
//小计
function countSub(trDom)
{
	var num=trDom.children(".cart_td_6").children("input").val();
	var sub=trDom.children(".cart_td_5").html()*num;
	trDom.children(".cart_td_7").html(sub);
	return sub;
}
//积分
function countIntegral(trDom)
{
	var num=trDom.children(".cart_td_6").children("input").val();
	var integral=trDom.children(".cart_td_4").html()*num;;
	return integral;
}
//计算积分、小计、总计
function countAll()
{
	var subAll=0;
	var integralAll=0;
	//$("input").attr("checked")=="checked"   选中结果
	$("input[name='cartCheckBox']").each(function(){
		var tr=$(this).parent().parent();
		var sub=countSub(tr);
		var integral=countIntegral(tr);
		if($(this).attr("checked")=="checked")
		{
			subAll+=sub;
			integralAll+=integral;
		}
	});
	$("#total").html(subAll);
	$("#integral").html(integralAll);
	
}
//加减
function hand($hand)
{
		var alt=$hand.attr("alt");
		var num=$hand.siblings("input");
		if(alt=="add")
		{
			num.val(parseInt(num.val())+1);
		}
		else
		{
			if(num.val()>1)
			{
				num.val(parseInt(num.val())-1);
			}
			else
			{
				alert("商品数量必须大于0");	
			}
		}
		countAll();       //加减后重新计算积分和价格	
}
//全选
function checkAll()
{
	$("input[name='cartCheckBox']").attr("checked",$("#allCheckBox").get(0).checked);
	countAll();     //全选后重新计算积分和价格
}