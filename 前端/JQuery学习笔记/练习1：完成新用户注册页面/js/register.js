$(function(){
	$("#userName").hover(function(){
		$("#userNameId").removeClass().html("请输入您常用的电子邮箱");
	},function(){
		test_userName();
	});
	$("#pwd").hover(function(){
		$("#pwdId").removeClass().html("请输入您的密码");
	},function(){
		test_pwd();
	});
	$("#repwd").hover(function(){
		$("#repwdId").removeClass().html("请输入您的密码");
	},function(){
		test_repwd();
	});
	$("#nickName").hover(function(){
		$("#nickNameId").removeClass().html("请输入您的昵称");
	},function(){
		test_nickName();
	});
	$("#tel").hover(function(){
		$("#telId").removeClass().html("请输入您的手机号");
	},function(){
		test_tel();
	});
	$("#email").hover(function(){
		$("#emailId").removeClass().html("请输入您的手机号");
	},function(){
		test_email();
	});
});
function test_userName()
{
	var thisIn=$("#userName");
	var nextDiv=$("#userNameId");
	var reg=/^[0-9a-zA-Z][0-9a-zA-Z_.-]{2,6}[0-9a-zA-Z]$/;
	if(thisIn.val()=="")
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("通行证用户名不能为空，请输入通行证用户名");
	}
	else if(!reg.test(thisIn.val()))
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("1、由字母、数字、下划线、点、减号组成<br/>2、只能以数字、字母开头或结尾，且长度为4-18");
	}
	else
	{
		nextDiv.addClass("ok_prompt").removeClass("error_prompt").html("通行证输入正确");
	}
}

function test_pwd()
{
	var thisIn=$("#pwd");
	var nextDiv=$("#pwdId");
	var reg=/^\S{6,16}$/;
	if(thisIn.val()=="")
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("密码不能为空，请输入密码");
	}
	else if(!reg.test(thisIn.val()))
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("密码长度为6-16位");
	}
	else
	{
		nextDiv.addClass("ok_prompt").removeClass("error_prompt").html("密码输入正确");
	}
}

function test_repwd()
{
	var thisIn=$("#repwd");
	var nextDiv=$("#repwdId");
	if(thisIn.val()=="")
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("重复密码不能为空，请重复输入密码");
	}
	else if(thisIn.val()!=$("#pwd").val())
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("重复密码与密码不匹配，请确认后输入");
	}
	else
	{
		nextDiv.addClass("ok_prompt").removeClass("error_prompt").html("重复密码输入正确");
	}
}

function test_nickName()
{
	var thisIn=$("#nickName");
	var nextDiv=$("#nickNameId");
	var reg=/^[\u4e00-\u9fa5]|\w|[@#$%&*]+$/;
	if(thisIn.val()=="")
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("昵称不能为空，请输入昵称");
	}
	else if((!reg.test(thisIn.val()))||thisIn.val().replace(/[\u4e00-\u9fa5]/g,"XX").length<4||thisIn.val().replace(/[\u4e00-\u9fa5]/g,"XX").length>20)
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("1、昵称由汉字、字母、数字、下划线、以及@、!、#、$、%、&、*特殊字符组成<br/>2、长度为4-20个字符，一个汉字占两个字符。");
	}
	else
	{
		nextDiv.addClass("ok_prompt").removeClass("error_prompt").html("昵称输入正确");
	}
}

function test_tel()
{
	var thisIn=$("#tel");
	var nextDiv=$("#telId");
	var reg=/^(13|15|18)[0-9]{9}$/;
	if(thisIn.val()=="")
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("关联手机号不能为空，请输入您的手机号");
	}
	else if(!reg.test(thisIn.val()))
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("关联手机号由11位数字组成，由13、15、18开头");
	}
	else
	{
		nextDiv.addClass("ok_prompt").removeClass("error_prompt").html("关联手机号输入正确");
	}
}
function test_email()
{
	var thisIn=$("#email");
	var nextDiv=$("#emailId");
	var reg=/^\w+@\w+(\.[a-zA-Z]{2,3}){1,2}$/;
	if(thisIn.val()=="")
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("保密邮箱不能为空，请输入您的保密邮箱");
	}
	else if(!reg.test(thisIn.val()))
	{
		nextDiv.addClass("error_prompt").removeClass("ok_prompt").html("邮箱的格式如 web@sina.com.cn或web@tom.com");
	}
	else
	{
		nextDiv.addClass("ok_prompt").removeClass("error_prompt").html("保密邮箱输入正确");
	}
}