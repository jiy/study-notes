$(function(){
	//使用tbody tr选择器返回的是一个长度为5的数组，数组下标从0开始
	//:even是JQuery中的过滤器，过滤掉所有奇数的元素（注意：下标从0开始）
	//因为包含“学号”、“姓名”般表头在css中已定义背景色，所以不受影响
    $("tbody tr:even").css("background-color","#FFFFCC");
	
	//选择所有学号的单元格
	var numTd=$("tbody td:even");
	
	//测试是否选择正确
	//num.css("background-color","red");
	
	//注册单击事件
	numTd.click(function(){
	    
		//找到当前点击的td
		var tdObj=$(this);
		
		if(tdObj.children("input").length>0)
		{
			//如果当前td中已存在input，不做处理
			//否则双击时会出现bug
			return false;
		}
		
		//获取td原始内容
		var text=tdObj.html();
		
		//清空td内容
		tdObj.html("");
		
		//创建一个文本框
		//去掉文本框边框
		//设置文本框字体大小为16px
		//使文本框的宽度和td宽度相同
		//设置文本框背景色为当前td背景色
		//将td的内容添加到文本框
		//appendTo()与append()使用相反，是将当前对象附加到参数中的对象中
		var inputObj=$('<input type="text">').css("border-width","0").css("font-size","16px").width(tdObj.width()).css("background-color",tdObj.css("background-color")).val(text).appendTo(tdObj);
		
		//使文本框插入之后被选中
		//get()方法可以获取DOM对象
		inputObj.get(0).select();
		
		//处理回车和esc事件
		inputObj.keyup(function(event){
			//获取键值
			var key=event.which;
			//回车
			if(key==13)
			{
				//获取当前文本框内容
				var inputText=inputObj.val();
				
				//设置td内容为文本框内容
				tdObj.html(inputText);
			}
			//esc
			if(key==27)
			{
				//设置td内容为原始内容
				tdObj.html(text);
			}
		});
	});
});