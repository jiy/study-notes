$(document).ready(function(){         //页面加载完成时执行
	var txt=$("#txt1");                //通过id选择器获取输入框
    txt.keyup(function(){             //键盘按下事件
	    if(txt.val() == "")            //当文本框内容为空时加载类选择器:txt,不为空时移除类选择器:txt
	    {
	    	txt.addClass("txt");
	    } else{
	    	txt.removeClass("txt");
	    }
	});
	
	$("#btn1").click(function(){      //按钮单击事件
        if(txt.val() =="")             //文本框值为空
		{
			alert("不能提交空值！");
		} else
		{
			$("#def").html("输入的值是："+txt.val());   //通过id获取div,并写入html
		}
	});
    
});
