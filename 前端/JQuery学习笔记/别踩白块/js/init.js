var canvas;			//画布div

$(function(){
	canvas=$("#canvas");
	init();
});

/*初始化方法*/
function init()
{	
	canvas.html("");
	for(var n=0;n<4;n++)
	{
		addLine();
	}
}

/*添加一行白块*/
function addLine()
{
	var i=Math.round(Math.random()*100)%4;
	var line="";
	line+="<tr>";
	for(var n=0;n<4;n++)
	{
		if(n!=i)
		{
			line+="<td style='border:#FFF 1px solid;width:25%;height:100px;background:#CCC;'></td>";
		}
		else
		{
			line+="<td class='blank' style='border:#FFF 1px solid;width:25%;height:100px;background:#555;'></td>";
		}
	}
	line+="</tr>";
	canvas.append(line);
}

//移除第一行
function removeFirst()
{
	canvas.children().children().eq(0).remove();
}

//白块单击事件
$("td").live("click", function(){
    var thistd=$(this);
	if(thistd.attr("class")=="blank")
	{
		//单击到黑块
		if(thistd.parent().index()==0)
		{
			//第一行
			removeFirst();
			addLine();
		}
		else
		{
			//其它行
			alert("游戏结束");
			init();
		}
	}
	else
	{
		//单击到白块
		alert("游戏结束");
		init();
	}
});