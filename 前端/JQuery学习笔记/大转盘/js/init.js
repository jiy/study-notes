/**
*该代码由无痕编写于2015-5-12，仅供学习交流，一切使用后果与本人无关
*本人QQ:2632790902   欢迎寻找错误
*/

var canvas;
var context;
var isClick=false;  
var colors=['rgb(254,238,200)',
			'rgb(130,100,10)',
			'rgb(92,139,210)'];
var txts=['测试1',
			'测试2',
			'测试3',
			'测试4',
			'测试5',
			'测试6',
			'测试7',
			'测试8',
			'测试9',
			'测试10',
			'测试11',
			'测试12'];
var ran;
var n=0;            //当前抽到的物品的index
$(document).ready(function(e) {
	canvas=document.getElementById('canvas');
	context =canvas.getContext("2d");
	context.translate(300, 300);    // 位移到圆心，方便绘制
	drawDisk(Math.PI/12);
	var shiftRad=Math.PI/12;
	            //表示点击状态，防止重复点击
	$('#button').click(function(){
		
		//防止重复点击
		if(isClick==true)
		{
			return;
		}
		isClick=true;
		
		ran=Math.floor((Math.random()*1000)%72);   //随机数，随机转动ran下
	    rotate(shiftRad,ran);                      //转动
	    
	    n=(Math.floor(ran%12)+n)%12;               //物品index值
	    
	    //转盘停止后0.5s显示消息
	    setTimeout(function(){
	    	alert('恭喜你获得：'+txts[n]);
	    },ran*50+500);
	});
});

//转动函数
function rotate(shiftRad,num)
{
	if(num<=0)
	{
		isClick=false;
		return;
	}
	setTimeout(function(){
		var i=shiftRad+Math.PI/6;
		drawDisk(i);
		rotate(i,--num);
	}, 50);	
}

/**
*绘制圆盘
*参数 偏移的弧度
*/
function drawDisk(shiftRad)
{
	
	var s=9,e=10;           //开始和结束画弧的地方
	for(var i=0;i<12;i++)
	{
		drawSector(context,colors[i%3],[0,0,250,Math.PI/6*s+shiftRad,Math.PI/6*e+shiftRad]);
	    s=(s==11)?0:s+1;
	    e=(e==11)?0:e+1;
	}
	context.rotate( Math.PI / 180 *30 );   //旋转文字
	for(var i=0;i<12;i++)
	{
		drawText(context,'#000000',txts[i]);
	}
}

/**
*绘制扇形
*参数 上下文，颜色值，位置参数
*/
function drawSector(ctx,color,arc)
{
	ctx.beginPath();            // 开始一条新路径
	ctx.moveTo(0, 0);           // 移动到圆心
	ctx.arc.apply( ctx, arc );
	ctx.fillStyle = color;
	ctx.closePath();            // 闭合路径
	ctx.fill();
	ctx.stroke();
}

/**
*绘制文本
*/
function drawText(ctx,color,txt)
{
	ctx.beginPath();
    ctx.moveTo(0,0)
    ctx.fillStyle= color;
    ctx.font= "16px 微软雅黑";
    ctx.textAlign="center";
    ctx.textBaseline="middle";
    ctx.rotate( Math.PI / 180 * -30 );   //旋转
    ctx.fillText( txt ,0, -160 );
    ctx.closePath();
    ctx.fill();
}