//1、获取XMLHttpRequest

var xhr;

if(window.XMLHttpRequest)
{
	xhr = new XMLHttpRequest();
}
else
{
	//兼容IE6
	xhr = new ActiveXObject("Microsoft.XMLHTTP");
}

//2、设置回调函数

xhr.onreadystatechange = function(){
	
	//如果请求成功
	if(xhr.readyState == 4 && xhr.status == 200)
	{
		//得到响应内容
		var  txt = xhr.responseText;
		alert(txt);
	}
};

//3、配置get请求
xhr.open("get","URL地址",true/*是否为异步请求*/);
//xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

//4、发送请求
xhr.send(null);