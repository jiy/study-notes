var pageIndex = 1;

function next()
{
    if(pageIndex == 6)
        return;
    var page = document.getElementById("page" + pageIndex);
    page.style.webkitTransform = "rotateX(-90deg)";
    pageIndex ++;
    var page = document.getElementById("page" + pageIndex);
    page.style.webkitTransform = "rotateX(0deg)";
}

function pre()
{
    if(pageIndex == 1)
        return;
    var page = document.getElementById("page" + pageIndex);
    page.style.webkitTransform = "rotateX(90deg)";
    pageIndex --;
    var page = document.getElementById("page" + pageIndex);
    page.style.webkitTransform = "rotateX(0deg)";
}