--判断数据库是否存在 START
	USE master			--设置当前数据库为master,方便访问sysdatabases表
	GO					--与后面语句区分开
	IF EXISTS (SELECT * FROM sysdatabases WHERE name = 'MySchool')	--判断数据库是否存在
		DROP DATABASE MySchool				--删除数据库
	GO
--END 判断数据库是否存在


--创建数据库 START
	CREATE DATABASE MySchool
		ON PRIMARY		--数据文件
		(
			NAME = 'MySchool_db',					--主数据文件逻辑名称
			FILENAME = 'F:\DATA\MySchool_db.mdf',	--主数据文件物理名称
			SIZE = 5MB,								--主数据文件初始化大小
			MAXSIZE = 20MB,							--主数据文件最大大小
			FILEGROWTH = 15%						--主数据文件增长率
		)
		LOG ON		--日志文件
		(
			--日志文件具体描述,各参数含义同上

			NAME = 'MySchool_log',					--日志文件逻辑名称
			FILENAME = 'F:\DATA\MySchool_log.ldf',	--日志文件物理名称
			SIZE = 2MB,								--日志文件初始化大小
			FILEGROWTH = 15%						--日志文件增长率
		)
	GO
--END 创建数据库

--设置当前表为MySchool
USE MySchool
GO

--创建表 START
	--Subject START
		IF EXISTS (SELECT * FROM sysobjects WHERE name='Subject')	--判断表是否存在
			DROP TABLE Subject					--删除表
		GO

		--创建课程表Subject
		CREATE TABLE Subject
		(
			SubjectNo int PRIMARY KEY IDENTITY(1,1),	
											--课程编号,非空(标识列,从1开始递增1)
			SubjectName nvarchar(50) NULL,	--课程名称
			ClassHour int NULL,				--学时
			GradeID int NULL				--年纪编号
		)
		GO	
	--END Subject


	--Result START
		IF EXISTS (SELECT * FROM sysobjects WHERE name='Result')	--判断表是否存在
		DROP TABLE Result					--删除表
		GO

		--创建成绩表Result
		CREATE TABLE Result
		(
			StudentNo int NOT NULL,			--学号
			SubjectNo int NOT NULL,			--课程编号
			ExamDate datetime NOT NULL,		--考试日期
			StudentResult int NOT NULL		--考试成绩
		)
		GO
	--END Result


	--Student START
		IF EXISTS (SELECT * FROM sysobjects WHERE name='Student')	--判断表是否存在
			DROP TABLE Student					--删除表
		GO

		--创建学生信息表
		CREATE TABLE Student
		(
			StudentNo int NOT NULL,				--学号,非空
			LoginPwd nvarchar(20) NOT NULL,		--登录密码,非空
			StudentName nvarchar(20) NOT NULL,	--学生姓名,非空
			Sex bit NOT NULL,					--学生性别,取值0或1
			GradeID int NOT NULL,				--年级编号,非空
			Phone varchar(50) NULL,				--电话
			Address varchar(255) NULL,			--地址
			BornDate datetime NOT NULL,		--出生日期,非空
			Email varchar(50) NULL,				--邮件帐号
			IdentityCard varchar(18) NOT NULL	--身份证号,非空
		)
		GO	
	--END Student


	--Grade START
		IF EXISTS (SELECT * FROM sysobjects WHERE name='Grade')	--判断表是否存在
			DROP TABLE Grade					--删除表
		GO

		--创建年级表
		CREATE TABLE Grade
		(
			GradeID int  PRIMARY KEY IDENTITY(1,1),
												--年级编号,非空(标识列,从1开始自增1)
			GradeName nvarchar(50) NOT NULL		--年级名称,非空
		)
		GO	
	--END Grade

--END 创建表


--创建约束 START
	--成绩表(Result)约束
		ALTER TABLE Result
			ADD CONSTRAINT FK_SubjectNo FOREIGN KEY(SubjectNo) REFERENCES Subject(SubjectNo)	--成绩表&课程表(Subject)
		ALTER TABLE Result
			ADD CONSTRAINT FK_StudentNo FOREIGN KEY(StudentNo) REFERENCES Student(StudentNo)	--成绩表&学生表(Student)
	--学生信息表(Student)约束
		ALTER TABLE Student
			ADD CONSTRAINT PK_StudentNo PRIMARY KEY(StudentNo)									--设置学号(StudentNo)为主键
		ALTER TABLE Student
			ADD CONSTRAINT FK_GradeIDForStudent FOREIGN KEY(GradeID) REFERENCES Grade(GradeID)			--学生信息表&年级表(Grade)
	--课程表(Subject)约束
		ALTER TABLE Subject
			ADD CONSTRAINT FK_GradeID2ForSubject FOREIGN KEY(GradeID) REFERENCES Grade(GradeID)			--课程表&年级表(Grade)
	GO
--END 创建约束