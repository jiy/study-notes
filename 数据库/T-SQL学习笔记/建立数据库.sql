--创建数据库
CREATE DATABASE MySchool
	ON PRIMARY		--数据文件
	(
		NAME = 'MySchool_db',					--主数据文件逻辑名称
		FILENAME = 'F:\DATA\MySchool_db.mdf',	--主数据文件物理名称
		SIZE = 5MB,								--主数据文件初始化大小
		MAXSIZE = 20MB,							--主数据文件最大大小
		FILEGROWTH = 15%						--主数据文件增长率
	)
	LOG ON		--日志文件
	(
		--日志文件具体描述,各参数含义同上

		NAME = 'MySchool_log',					--日志文件逻辑名称
		FILENAME = 'F:\DATA\MySchool_log.ldf',	--日志文件物理名称
		SIZE = 2MB,								--日志文件初始化大小
		FILEGROWTH = 15%						--日志文件增长率
	)
GO