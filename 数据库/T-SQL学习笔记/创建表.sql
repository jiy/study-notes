USE MySchool
GO
IF EXISTS (SELECT * FROM sysobjects WHERE name='Student')	--判断表是否存在
	DROP TABLE Student					--删除表
GO
--创建学生表
CREATE TABLE Student
	(
		StudentNo int NOT NULL,				--学号,非空
		LoginPwd nvarchar(20) NOT NULL,		--登录密码,非空
		StudentName nvarchar(20) NOT NULL,	--学生姓名,非空
		Sex bit NOT NULL					--学生性别,取值0或1
	)
GO	
