--我的租房网项目	2015/07/15
--BY：无痕

--建立数据库及表结构 START
	--判断数据库是否存在 START
		USE master			--设置当前数据库为master,方便访问sysdatabases表
		GO					--与后面语句区分开
		IF EXISTS (SELECT * FROM sysdatabases WHERE name = 'House')	--判断数据库是否存在
			DROP DATABASE House				--删除数据库
		GO
	--END 判断数据库是否存在

	--创建数据库 START
		CREATE DATABASE House
			ON PRIMARY		--数据文件
			(
				NAME = 'House_db',					--主数据文件逻辑名称
				FILENAME = 'F:\DATA\House_db.mdf',	--主数据文件物理名称
				SIZE = 5MB,								--主数据文件初始化大小
				MAXSIZE = 20MB,							--主数据文件最大大小
				FILEGROWTH = 15%						--主数据文件增长率
			)
			LOG ON		--日志文件
			(
				--日志文件具体描述,各参数含义同上

				NAME = 'House_log',					--日志文件逻辑名称
				FILENAME = 'F:\DATA\House_log.ldf',	--日志文件物理名称
				SIZE = 2MB,								--日志文件初始化大小
				FILEGROWTH = 15%						--日志文件增长率
			)
		GO
	--END 创建数据库

	--设置当前表为House
	USE House
	GO

	--创建表 START
	
		--sys_user START
			IF EXISTS (SELECT * FROM sysobjects WHERE name='sys_user')	--判断表是否存在
				DROP TABLE sys_user					--删除表
			GO

			--创建用户表sys_user
			CREATE TABLE sys_user
			(
				UID int PRIMARY KEY IDENTITY(1,1),	
													--客户编号,主键,标识列(从1开始递增1)
				UName varchar(25) NOT NULL,			--客户姓名,非空
				UPASSWORD varchar(25) NOT NULL,		--密码,至少6个字符
			)
			GO	
		--END sys_user

		--hos_district START
			IF EXISTS (SELECT * FROM sysobjects WHERE name='hos_district')	--判断表是否存在
				DROP TABLE hos_district					--删除表
			GO

			--创建区县信息表hos_district
			CREATE TABLE hos_district
			(
				DID int PRIMARY KEY IDENTITY(1,1),	
													--区县编号,主键,标识列(从1开始递增1)
				DName varchar(25) NOT NULL			--区县名称,非空
			)
			GO	
		--END hos_district

		--hos_street START
			IF EXISTS (SELECT * FROM sysobjects WHERE name='hos_street')	--判断表是否存在
				DROP TABLE hos_street					--删除表
			GO

			--创建街道信息表hos_street
			CREATE TABLE hos_street
			(
				SID int PRIMARY KEY IDENTITY(1,1),	
													--街道编号,主键,标识列(从1开始递增1)
				SName varchar(25) NOT NULL,			--街道名称,非空
				SDID int NOT NULL					--区县编号,非空,外键
			)
			GO	
		--END hos_street

		--hos_type START
			IF EXISTS (SELECT * FROM sysobjects WHERE name='hos_type')	--判断表是否存在
				DROP TABLE hos_type					--删除表
			GO

			--创建房屋类型表hos_type
			CREATE TABLE hos_type
			(
				HTID int PRIMARY KEY IDENTITY(1,1),	
													--房屋类型编号,主键,标识列(从1开始递增1)
				HTName varchar(25) NOT NULL				--房屋类型名称,非空
			)
			GO	
		--END hos_type

		--hos_house START
			IF EXISTS (SELECT * FROM sysobjects WHERE name='hos_house')	--判断表是否存在
				DROP TABLE hos_house					--删除表
			GO

			--创建出租房屋信息表hos_house
			CREATE TABLE hos_house
			(
				HMID int PRIMARY KEY IDENTITY(1,1),	
														--初中房屋编号,主键,标识列(从1开始递增1)
				UID int NOT NULL,						--客户编号,非空,外键
				SID int NOT NULL,						--街道编号,非空,外键
				HTID int NOT NULL,						--房屋类型编号,非空,外键
				PRICE decimal NOT NULL,					--每月租金,非空,默认值为0,要求大于等于0
				TOPIC varchar(100) NOT NULL,			--标题,非空
				CONTENTS varchar(200) NOT NULL,			--描述,非空
				HTIME datetime NOT NULL,				--发布时间,非空,默认为当前日期,要求小于等于当前日期
				COPY varchar(200) NULL					--备注
			)
			GO	
		--END hos_house

	--END 创建表

	--添加约束 START
		--客户信息表(sys_user)约束
			ALTER TABLE sys_user
				ADD CONSTRAINT CK_UPASSWORD CHECK (LEN(UPASSWORD) >= 6)					--客户密码长度大于等于6
	
		--街道信息表(hos_street)约束
			ALTER TABLE hos_street
				ADD CONSTRAINT FK_SDID FOREIGN KEY (SDID) REFERENCES hos_district(DID)	--连接街道信息表和区县信息表(hos_district)
	
		--出租房屋信息表(hos_house)
			--默认约束
				ALTER TABLE hos_house 
					ADD CONSTRAINT DF_PRICE DEFAULT (0) FOR PRICE							--设置租金默认值为0
				ALTER TABLE hos_house 
					ADD CONSTRAINT DF_HTIME DEFAULT (GETDATE()) FOR HTIME					--设置发布时间默认值为当前日期
			--检查约束
				ALTER TABLE hos_house 
					ADD CONSTRAINT CK_PRICE CHECK (PRICE >= 0)								--租金必须大于等于0
				ALTER TABLE hos_house 
					ADD CONSTRAINT CK_HTIME CHECK (HTIME <= GETDATE())						--发布时间必须小于等于当前日期
			--外键约束
				ALTER TABLE hos_house 
					ADD CONSTRAINT FK_UID FOREIGN KEY (UID) REFERENCES sys_user(UID)		--连接出租房屋信息表与客户信息表(sys_user)
				ALTER TABLE hos_house 
					ADD CONSTRAINT FK_SID FOREIGN KEY (SID) REFERENCES hos_street(SID)		--连接出租房屋信息表与街道信息表(hos_street)
				ALTER TABLE hos_house 
					ADD CONSTRAINT FK_HTID FOREIGN KEY (HTID) REFERENCES hos_type(HTID)		--连接出租房屋信息表与房屋类型表(hos_type)
	--END 添加约束

--END 建立数据库及表结构

--添加测试数据 START
	--添加客户信息表数据
		INSERT INTO [dbo].[sys_user] ([UName],[UPASSWORD]) VALUES ('张总','123456')
		INSERT INTO [dbo].[sys_user] ([UName],[UPASSWORD]) VALUES ('李总','123456')
		INSERT INTO [dbo].[sys_user] ([UName],[UPASSWORD]) VALUES ('王总','123456')
		INSERT INTO [dbo].[sys_user] ([UName],[UPASSWORD]) VALUES ('赵总','123456')
		INSERT INTO [dbo].[sys_user] ([UName],[UPASSWORD]) VALUES ('孙总','123456')

	--添加区县信息表数据
		INSERT INTO [dbo].[hos_district] ([DName]) VALUES ('嘉陵区')
		INSERT INTO [dbo].[hos_district] ([DName]) VALUES ('顺庆区')
		INSERT INTO [dbo].[hos_district] ([DName]) VALUES ('高坪区')
		
	--添加街道信息表数据
		INSERT INTO [dbo].[hos_street] ([SName],[SDID]) VALUES ('火花路',1)
		INSERT INTO [dbo].[hos_street] ([SName],[SDID]) VALUES ('都尉路',1)
		INSERT INTO [dbo].[hos_street] ([SName],[SDID]) VALUES ('文化路',2)
		INSERT INTO [dbo].[hos_street] ([SName],[SDID]) VALUES ('涪江路',2)
		INSERT INTO [dbo].[hos_street] ([SName],[SDID]) VALUES ('龙门',3)
		INSERT INTO [dbo].[hos_street] ([SName],[SDID]) VALUES ('白塔路',3)

	--添加房屋类型表数据
		INSERT INTO [dbo].[hos_type] ([HTName]) VALUES ('两室一厅')
		INSERT INTO [dbo].[hos_type] ([HTName]) VALUES ('两室一卫')
		INSERT INTO [dbo].[hos_type] ([HTName]) VALUES ('一室一厅')
		INSERT INTO [dbo].[hos_type] ([HTName]) VALUES ('一室一卫')


	--添加出租房屋信息表表数据
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,1,1,2500,'租房标题','租房信息','2015-01-02')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,2,2,2000,'租房标题','租房信息','2015-02-05')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,3,1,1800,'租房标题','租房信息','2015-03-08')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,4,3,500,'租房标题','租房信息','2015-01-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,5,2,1900,'租房标题','租房信息','2015-05-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,6,4,1200,'租房标题','租房信息','2015-06-15')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,1,1,2500,'租房标题','租房信息','2015-06-02')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,2,2,2000,'租房标题','租房信息','2015-07-05')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,3,1,1800,'租房标题','租房信息','2015-07-08')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,4,3,500,'租房标题','租房信息','2015-07-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,5,2,1900,'租房标题','租房信息','2015-07-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,6,4,1200,'租房标题','租房信息','2015-07-15')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,1,1,2500,'租房标题','租房信息','2015-07-02')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,2,2,2000,'租房标题','租房信息','2015-07-05')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,3,1,1800,'租房标题','租房信息','2015-07-08')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,4,3,500,'租房标题','租房信息','2015-07-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,5,2,1900,'租房标题','租房信息','2015-07-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,6,4,1200,'租房标题','租房信息','2015-07-15')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,1,1,2500,'租房标题','租房信息','2015-07-02')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,2,2,2000,'租房标题','租房信息','2015-07-05')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,3,1,1800,'租房标题','租房信息','2015-07-08')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (1,4,3,500,'租房标题','租房信息','2015-07-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (2,5,2,1900,'租房标题','租房信息','2015-07-12')
		INSERT INTO [dbo].[hos_house] ([UID],[SID],[HTID],[PRICE],[TOPIC],[CONTENTS],[HTIME]) VALUES (3,6,4,1200,'租房标题','租房信息','2015-07-15')

--END 添加测试数据

--SQL编程练习 START
	USE House
	GO
	
	--需求1:分页显示查询出租房屋信息
		--需求说明：查询输出第6条～第10条出租房屋信息
		--提示：
			--使用临时表保存租房记录
			--使用子查询和关键字TOP获取出租房的记录
		
		SELECT TOP 5 * FROM										--分页大小
			(SELECT * FROM hos_house)tempTable					--传说中的临时表
			WHERE tempTable.HMID NOT IN
			(SELECT TOP 5 h.HMID FROM hos_house h )				--分页依据:HMID
			
		GO


	--需求2:查询指定客户发布的出租房屋信息
		--需求说明:查询张总发布的所有出租房屋信息,显示区县、街道、户型编号、价格、标题、描述、时间和备注信息
		--提示：
			--1、用连接查询实现
			--2、用子查询实现

		--用连接查询实现
			SELECT hd.DName 区县,hs.SName 街道,hh.HTID 户型编号,hh.PRICE 价格,hh.TOPIC 标题,hh.CONTENTS 描述,hh.HTIME 时间,hh.COPY 备注 
			FROM hos_house hh
				JOIN sys_user su ON su.UID=hh.UID
				JOIN hos_street hs ON hs.SID=hh.SID
				JOIN hos_district hd ON hd.DID=hs.SDID
			WHERE su.UName = '张总'

			GO

		--用子查询实现
			SELECT 
				(SELECT hd.DName FROM hos_district hd WHERE hd.DID =					--根据区县编号查询区县名称
					(SELECT hs.SDID FROM hos_street hs WHERE hs.SID= hh.SID)			--根据街道编号查出对应的区县编号
				) 区县,
				(SELECT hs.SName FROM hos_street hs WHERE hs.SID = hh.SID ) 街道,		--根据街道编号查询街道名称
				hh.HTID 户型编号,
				hh.PRICE 价格,
				hh.TOPIC 标题,
				hh.CONTENTS 描述,
				hh.HTIME 时间,
				hh.COPY 备注 
			FROM hos_house hh
			WHERE hh.UID = (SELECT su.UID FROM sys_user su WHERE su.UName = '张总' )	--筛选条件:根据客户姓名查询客户编号

			GO


	--需求3:按区县制作房屋出租清单
		--需求说明:根据户型和房屋所在区县和街道,为至少有两个街道有出租房屋的区县制作出房屋清单,显示户型、姓名、区县、街道
			--提示：使用HAVING子句筛选出街道数量大于1的区县

		SELECT ht.HTName 户型,su.UName 姓名,hd.DName 区县,hs.SName 街道
		FROM hos_house hh
		--连表
		JOIN hos_type ht ON ht.HTID = hh.HTID
		JOIN sys_user su ON su.UID = hh.UID
		JOIN hos_street hs ON hs.SID = hh.SID
		JOIN hos_district hd ON hd.DID = hs.SDID
		WHERE 
			hd.DID IN (
				SELECT hd.DID 
				FROM hos_house hh 
				JOIN hos_street hs ON hs.SID = hh.SID 
				JOIN hos_district hd ON hd.DID = hs.SDID 
				GROUP BY hd.DID HAVING SUM(hs.SID)>1			--根据区县编号分组,并筛选街道数大于1的区县
			)

		GO

	--需求4：按季度统计本年发布的房屋出租量
		--需求说明：要求输出本年从1月1日起至今的全部出租房屋数量、各区县出租房屋数量及各街道、户型出租房屋数量
		--提示
			--1、使用变量保存当年的年份
			--2、使用子查询和连接查询统计各区县、各街道当年各季度的房屋出租数量
			--3、使用联合查询输出各季度总量

		DECLARE @year int					--储存当前年份
		SET @year = YEAR(GETDATE());		--获得当前年份

        SELECT 
			temp.jd 季度, hd.DName 区县, hs.SName 街道, ht.HTName 房屋类型, temp.num  数量
			FROM (
				SELECT  DATEPART(qq,hh.HTIME) jd,hh.SID , hh.HTID, count(*) AS num
					FROM hos_house hh 
					WHERE  YEAR(hh.HTIME)=@year									--筛选今年的
					GROUP BY DATEPART(qq,hh.HTIME), hh.SID, hh.HTID				--按照季度、街道和住房类型名称分组
				) temp		--创建临时表

			--连表
		   	JOIN hos_type ht ON ht.HTID = temp.HTID
			JOIN hos_street hs ON hs.SID = temp.SID
			JOIN hos_district hd ON hd.DID = hs.SDID
			
		UNION				--联合上下表

		SELECT DATEPART(qq,xj.HTIME), hd.DName, ' 小计','',count(*)  FROM  hos_house xj 
			
			--连表
			JOIN hos_street hs ON hs.SID = xj.SID
			JOIN hos_district hd ON hd.DID = hs.SDID
			WHERE  YEAR(xj.HTIME)=@year											--筛选今年的
			GROUP BY DATEPART(qq,xj.HTIME), hd.DName							--按照季度和区县名称分组
			
		UNION			--联合上下表

		SELECT DATEPART(qq,hj.HTIME), ' 合计', '','',count(*)  FROM  hos_house hj 
			JOIN hos_street hs ON hs.SID = hj.SID
			JOIN hos_district hd ON hd.DID = hs.SDID
			WHERE  YEAR(hj.HTIME)=@year											--筛选今年的
			GROUP BY DATEPART(qq,hj.HTIME)										--按照季度分组

		GO
			
--END SQL编程练习