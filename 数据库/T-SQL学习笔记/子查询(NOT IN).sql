USE MySchool
GO

--查询未参加SQL Base课程最近一次考试的学生名单
SELECT stu.StudentName FROM Student stu WHERE										--查询未参加该次考试学生姓名
stu.StudentNo NOT IN
	(SELECT r.StudentNo FROM Result r WHERE											--查询参加该课程最近一次考试的学生学号
	r.SubjectNo=		--课程编号
			(SELECT sj.SubjectNo FROM Subject sj WHERE sj.SubjectName='SQL Base') AND
	r.ExamDate =		--该课程考试时间
		(SELECT MAX(r.ExamDate) FROM Result r WHERE									--根据课程编号查询最近一次考试时间
		r.SubjectNo=
			(SELECT sj.SubjectNo FROM Subject sj WHERE sj.SubjectName='SQL Base')	--根据课程名称查询课程编号
		)
	)