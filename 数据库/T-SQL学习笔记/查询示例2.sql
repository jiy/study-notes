USE MySchool

DECLARE @No int				--学号
DECLARE @Name varchar(20)	--姓名
DECLARE @SubjectName varchar(20)
							--科目名称
DECLARE @Result int			--分数

SET @No=20011				--学号赋值
SET @SubjectName='Java Logic'
							--科目名称赋值

--查询最近一次考试分数					
SELECT TOP 1 @Result=dbo.Result.StudentResult, @Name=dbo.Student.StudentName FROM      
			dbo.Subject INNER JOIN
			dbo.Result ON dbo.Subject.SubjectNo = dbo.Result.SubjectNo INNER JOIN
            dbo.Student ON dbo.Result.StudentNo = dbo.Student.StudentNo
		WHERE dbo.Student.StudentNo=@No AND dbo.Subject.SubjectName=@SubjectName 
		order by dbo.Result.ExamDate desc					--排序（按日期降幂）

--显示姓名、年龄
PRINT '姓名：'+@Name

IF (@Result>85)
	BEGIN
		PRINT '优秀';
	END
ELSE IF(@Result>75)
	BEGIN
		PRINT '良好';
	END
ELSE IF(@Result>60)
	BEGIN
		PRINT '中等';
	END
ELSE
	BEGIN
		PRINT '差';
	END
