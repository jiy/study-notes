USE MySchool


--查询Java Logic的最后一次考试实到人数和应到人数

DECLARE @subName nvarchar(100)
DECLARE @examDate datetime

SET @subName='Java Logic'

SELECT @examDate=MAX(rs.ExamDate) FROM Result rs INNER JOIN 
	Subject sub ON sub.SubjectNo=rs.SubjectNo
WHERE sub.SubjectName=@subName

SELECT COUNT(*) 实到 FROM Result rs WHERE rs.ExamDate=@examDate

SELECT COUNT(sub.SubjectNo) 应到 FROM Student stu INNER JOIN
	Result rs ON rs.StudentNo=stu.StudentNo INNER JOIN
	Subject sub ON sub.SubjectNo = rs.SubjectNo
WHERE sub.GradeID=stu.GradeID



--提取Java Logic课程最近一次考试的成绩信息并保持结构。成绩信息包括学生姓名、学号、成绩和是否通过
--并保存为一张新表

--判断表是否存在,存在就删除
IF EXISTS(SELECT * FROM sysobjects WHERE name='TempResult')
	DROP TABLE TempResult

SELECT 
	stu.StudentName 'StudentName',
	stu.StudentNo 'StudentNo',
	rs.StudentResult 'StudentResult',
	(CASE WHEN rs.StudentResult>=60 THEN '是' ELSE '否' END) 'isPass' 
	
	INTO TempResult											--保存到新表
	FROM Student stu 
	INNER JOIN Result rs ON rs.StudentNo=stu.StudentNo 
	INNER JOIN Subject sub ON sub.SubjectNo = rs.SubjectNo
WHERE rs.ExamDate=@examDate



--根据考试平均分给地域平均分的学生加分
DECLARE @avg float

SELECT 
	@avg=AVG(rs.StudentResult) 
	FROM Student stu 
	INNER JOIN Result rs ON rs.StudentNo=stu.StudentNo 
	INNER JOIN Subject sub ON sub.SubjectNo = rs.SubjectNo 
WHERE rs.ExamDate=@examDate

--如果平均分低于60分，设置平均分为60分
IF(@avg<60)
	BEGIN
		SET @avg=60
	END

--循环给学生加分
WHILE(1=1)
	BEGIN
		UPDATE TempResult  SET StudentResult=StudentResult+1
		WHERE StudentResult<@avg AND StudentResult<97
	END
	
--跟新isPass列
UPDATE TempResult SET isPass = (CASE WHEN StudentResult>=60 THEN '是' ELSE '否' END)



--显示加分后学生最终成绩和通过率


--最终成绩
SELECT 
	(CASE WHEN tr.StudentResult IS NULL THEN '缺考' ELSE tr.StudentResult END) 成绩,
	tr.isPass 是否通过 
FROM TempResult tr


--通过率
SELECT 
	COUNT(*) 总人数,
	SUM((CASE WHEN tr.isPass='是' THEN 1 ELSE 0 END)) 通过人数,
	(CONVERT(varchar(5),SUM((CASE WHEN tr.isPass='是' THEN 1 ELSE 0 END))/COUNT(*)*100)+'%') 通过率 
FROM TempResult tr