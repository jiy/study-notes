USE MySchool
GO

--需求说明：利用索引查询考试成绩为80～90的所有记录。要求输出：学生姓名、课程名称、考试日期和考试成绩
--提示：
	--在Result的StudentResult字段上，创建可重复索引，并使用索引查询相关记录


--检查索引是否存在，存在则删除
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_Result_StudentResult')
	DROP INDEX Result.IX_Result_StudentResult
GO

--创建索引
CREATE NONCLUSTERED INDEX IX_Result_StudentResult
	ON Result(StudentResult)
		WITH FILLFACTOR=30
GO

SELECT stu.StudentName 姓名,sub.SubjectName 课程,rs.ExamDate 考试日期,rs.StudentResult 成绩 FROM Result rs
WITH (INDEX=IX_Result_StudentResult)				--使用索引
JOIN Student stu ON stu.StudentNo = rs.StudentNo
JOIN Subject sub ON sub.SubjectNo = rs.SubjectNo
WHERE rs.StudentResult >= 80 AND rs.StudentResult <= 90