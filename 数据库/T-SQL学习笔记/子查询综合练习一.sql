USE MySchool

--为每个学生制作各门课程成绩，要求每个学生参加每门课程最后一次成绩作为该学生本课程的最终成绩
--成绩包含学生姓名，课程所属年级名称，课程名称，考试日期，考试成绩

	--	一、通过联表查询实现
	
	SELECT  stu.StudentName '姓名',sub.SubjectName '科目',r.StudentResult '成绩',r.ExamDate '考试日期',(SELECT  g.GradeName  FROM  Grade g WHERE g.GradeID = stu.GradeID) '年级名称' FROM  Result  r

	JOIN Student  stu  ON r.StudentNo = stu.StudentNo
	JOIN Subject sub  ON sub.SubjectNo  = r.SubjectNo

	WHERE r.ExamDate  = (
	     SELECT  MAX(rr.ExamDate)  FROM Result  rr WHERE						--成绩表里面找每个学生每个科目最后的考试日期
		 rr.StudentNo = stu.StudentNo  AND rr.SubjectNo = sub.SubjectNo		
		 GROUP BY rr.SubjectNo
	)



	--	二、通过子查询实现
	
	SELECT 
		(SELECT s.StudentName  FROM Student s WHERE s.StudentNo = temp.StudentNo) '姓名',
		(SELECT b.SubjectName  FROM Subject b WHERE b.SubjectNo = temp.SubjectNo	) '科目名称' ,
		temp.maxdate,  
		(
			SELECT  r.StudentResult  FROM Result r WHERE r.StudentNo = temp.StudentNo 
			and r.SubjectNo = temp.SubjectNo 
			and r.ExamDate = temp.maxdate
		) '成绩' 
	FROM 
	(												
		SELECT  r.StudentNo, r.SubjectNo, MAX(r.ExamDate) as maxdate
		FROM  result  r  GROUP BY  r.StudentNo, r.SubjectNo
	)  temp																		--创建一张零时表
