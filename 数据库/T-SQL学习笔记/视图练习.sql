USE MySchool
GO 

--需求：统计每个学生各学期(S1、S2、Y2)所有课程总成绩
--提示：
	--1、创建视图，实现查询学生各个学期参加考试的总成绩，每门课程成绩以该学生参加的最后一次考试为准
	--2、编码查看视图运行结果，获得学生各学期考试的总成绩

	--如果该视图已经存在，则删除存在的视图
	IF EXISTS(SELECT * FROM sysobjects WHERE name='allResult')
		DROP VIEW allResult
	GO

	CREATE VIEW allResult			--新建视图
	AS
	SELECT stu.StudentName 姓名,stu.StudentNo 学号,stu.Phone 电话,g.GradeName 学期,SUM(rs.StudentResult) 成绩 FROM Result rs
	JOIN Student stu ON stu.StudentNo = rs.StudentNo
	JOIN Subject sub ON sub.SubjectNo = rs.SubjectNo
	JOIN Grade g ON g.GradeID = sub.GradeID
	WHERE rs.ExamDate=(
		SELECT MAX(rr.ExamDate) 
		FROM Result rr 
		WHERE rr.StudentNo=stu.StudentNo AND rr.SubjectNo=rs.SubjectNo
	)
	GROUP BY g.GradeName,stu.StudentNo,stu.StudentName,stu.Phone			--分组
	
	GO

	SELECT * FROM allResult			--查看视图