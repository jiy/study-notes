USE MySchool
GO 

--事务练习：为所有Y2学生办理毕业手续

BEGIN TRANSACTION				--事务开始

DECLARE @err int				--定义变量统计错误

--储存学生考试信息
SELECT * 
INTO HistoryResult				--保存到新表
FROM Result rs					--查询Y2相关科目的考试信息
WHERE rs.SubjectNo IN (
	SELECT sub.SubjectNo		--查询Y2的科目编号
	FROM Subject sub 
	WHERE sub.GradeID=(			--查询Y2的年级编号
		SELECT g.GradeID 
		FROM Grade g 
		WHERE g.GradeName='Y2'
	)
)

SET @err=@err+@@ERROR			--累计错误

--删除考试信息
DELETE FROM Result WHERE SubjectNo IN (
	SELECT sub.SubjectNo		--查询Y2的科目编号
	FROM Subject sub 
	WHERE sub.GradeID=(			--查询Y2的年级编号
		SELECT g.GradeID 
		FROM Grade g 
		WHERE g.GradeName='Y2'
	)
)

SET @err=@err+@@ERROR			--累计错误

--储存学生信息
SELECT * 
INTO HistoryStudent FROM Student stu WHERE stu.GradeID=(
	SELECT g.GradeID 
	FROM Grade g 
	WHERE g.GradeName='Y2'
)

SET @err=@err+@@ERROR			--累计错误

--删除学生信息
DELETE FROM Student WHERE GradeID=(
	SELECT g.GradeID 
	FROM Grade g 
	WHERE g.GradeName='Y2'
)

SET @err=@err+@@ERROR			--累计错误

IF(@err=0)						--如果没有错误
	BEGIN
		PRINT 'OK'
		COMMIT TRANSACTION		--提交事务0
	END
ELSE
	BEGIN
		PRINT '回滚'
		ROLLBACK TRANSACTION	--回滚(撤销)事务
	END
