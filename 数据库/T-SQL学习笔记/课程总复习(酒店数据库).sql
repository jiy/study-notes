use Hotel
go
-- 1、根据输入的客房类型名称，统计入住的客人总数
	
	if exists(select * from sysobjects where name = 'usp_s1')
		drop procedure usp_s1
	go

	create procedure usp_s1
		@typeName varchar(20) = null,
		@num int output
	as
		select @num = count(*) from GuestRecord gr
		join Room r on r.RoomID = gr.RoomID
		join RoomType rt on rt.TypeID = r.RoomTypeID
		where rt.TypeName = @typeName
	go

	declare @typeName varchar(20) = '标准间'
	declare @num int
	exec usp_s1 @typeName,@num output
	print @typeName+'客人总数'+':'+cast(@num as varchar(5))

-- 2、通过房间号查询客房编号、床位数、客房状态编号、客房描述、客房类型和房价，如果房间号为-1，则表示查询所有客房信息
	
	if exists(select * from sysobjects where name = 'usp_s2')
		drop procedure usp_s2
	go

	create procedure usp_s2
		@roomId int
	as
		if @roomId = -1
			begin
				select r.RoomID,r.BedNum,r.RoomStateID,r.Description,rt.TypeName,rt.TypePrice from GuestRecord gr
				join Room r on r.RoomID = gr.RoomID
				join RoomType rt on rt.TypeID = r.RoomTypeID	
			end
		else
			begin
				select r.RoomID,r.BedNum,r.RoomStateID,r.Description,rt.TypeName,rt.TypePrice from GuestRecord gr
				join Room r on r.RoomID = gr.RoomID
				join RoomType rt on rt.TypeID = r.RoomTypeID
				where r.RoomID = @roomId
			end
	go

	exec usp_s2 -1

--3、根据客房类型删除可客房类型记录。如果操作成功，则返回删除的记录数；否则返回-1。
	
	if exists(select * from sysobjects where name = 'usp_s3')
		drop procedure usp_s3
	go

	create procedure usp_s3
		@typeName varchar(20)
	as
		if not exists(select * from Room r join RoomType rt on r.RoomTypeID = rt.TypeID where rt.TypeName = @typeName)
			begin
				delete from RoomType where TypeName = @typeName
				return @@rowcount
			end
		else
			begin
				return -1
			end
	go

	declare @typeName varchar(20) = '三人间'
	declare @result int
	
	exec @result = usp_s3 @typeName

	if @result = -1
		print '删除记录失败'
	else
		print '删除'+@typeName+'的记录成功，受影响行数：'+cast(@result as varchar(5))
	go

-- 4、使用存储过程将入住客人信息插入到信息表中
	--需求说明：
		--身份证号必须由18个字符组成
		--押金默认值为1000元
		--将客人入住房间当前状态设置为“入住的状态编号”
		--如果客人记录插入成功，则输出客人流水号

	if exists(select * from sysobjects where name = 'usp_s4')
		drop procedure usp_s4
	go

	create procedure usp_s4
		@name varchar(20),			--客户姓名
		@identityId varchar(18),	--客户身份证号码
		@roomId int,				--入住房间号
		@resideDate datetime,		--入住日期
		@deposit decimal = 1000.0,	--押金(默认1000)
		@id	int output
	as
		if not len(@identityId) = 18
			return -1

		--事务处理开始
		begin transaction
		declare @errorSum int =0

		--更改客房人数
		update Room set GuestNum = GuestNum + 1 where RoomID = @roomId
		set @errorSum = @errorSum + @@ERROR
		--更改客房状态
		update Room set RoomStateID = 1 where RoomID = @roomId
		set @errorSum = @errorSum + @@ERROR
		--添加入住客户
		insert GuestRecord (IdentityID,GuestName,RoomID,ResideDate,Deposit,TotalMoney,ResideID) 
values (@identityId,@name,@roomId,@resideDate,@deposit,@deposit,1)
		set @errorSum = @errorSum + @@ERROR
		if @errorSum = 0
			begin
				print '提交事务'
				commit transaction
			end
		else
			begin
				print '事务回滚'
				rollback transaction
				return -1
			end
		--事务处理结束
		select @id = GuestID from GuestRecord where IdentityID = @identityId
		return 0
	go

	declare @result int
	declare @identityId varchar(18) = '111111111100000001'
	declare @name varchar(10) = '张三'
	declare @roomId int = 1008
	declare @residDate datetime = '2115-9-9 12:30:00'
	declare @deposit decimal = 1000
	declare @id int

	exec @result = usp_s4 @name,@identityId,@roomId,@residDate,@deposit,@id output

	if @result = 0
		begin
			print '插入客人记录操作成功'
			print '客人编号为：'+cast(@id as varchar(5))
		end
	else
		begin
			print '插入客人记录操作失败'
		end
