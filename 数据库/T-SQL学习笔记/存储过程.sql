use MySchool
go

--训练1 调用系统存储过程
	exec sp_columns 'Student'
	exec sp_helpconstraint ''
	exec sp_help
	go

--训练2 用无参存储过程查询各学期开设的课程名称和每门课程的课时
	
	if exists(select * from sysobjects where name='usp_s2')
		drop procedure usp_s2
	go

	create procedure usp_s2
	as
		SELECT   dbo.Grade.GradeName, dbo.Subject.SubjectName, dbo.Subject.ClassHour
		FROM     dbo.Grade JOIN
                 dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
	go

	exec usp_s2

	go

--训练3 查询输出指定学期总课时和开设的课程名称、课时
	
	if exists(select * from sysobjects where name='usp_s3')
		drop procedure usp_s3
	go

	create procedure usp_s3
	@gradeName varchar(5) = null
	as
		--如果没有传入年级信息，则显示所有年级
		if @gradeName is null
			begin
				select   dbo.Grade.GradeName, dbo.Subject.SubjectName, dbo.Subject.ClassHour
				from     dbo.Grade JOIN
						 dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
				union		--聚合函数，聚合上下表
				select  dbo.Grade.GradeName, '', sum(dbo.Subject.ClassHour)
				from     dbo.Grade JOIN
						 dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
				group by dbo.Grade.GradeName
			end
		--如果有传入年级信息，按照年级信息查询
		else
			begin
				select   dbo.Grade.GradeName, dbo.Subject.SubjectName, dbo.Subject.ClassHour
				from     dbo.Grade JOIN
						 dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
				where dbo.Grade.GradeName = @gradeName
				union
				select  dbo.Grade.GradeName, '', sum(dbo.Subject.ClassHour)
				from     dbo.Grade JOIN
						 dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
				where dbo.Grade.GradeName = @gradeName
				group by dbo.Grade.GradeName
			end
	go

	exec usp_s3 'S2'
	exec usp_s3

	go

--训练4 使用输入、输出参数的存储过程完成数据查询，获得相关数据
	--需求说明：
		--查询获得指定学期开设的课程名称和课时
		--如果学期名称为空，则显示“学期名称不能为空”，并返回
		--统计该学期的课程数和总课时

	if exists(select * from sysobjects where name='usp_s4')
		drop procedure usp_s4
	go

	create procedure usp_s4
	@gradeName varchar(5) = null,
	@CourseNum int output,
	@HourNum int output
	as
		if @gradeName is null
			print '学期名称不能为空'
		else
			begin
				select GradeName,SubjectName,ClassHour 
				from dbo.Grade 
				join dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
				where GradeName = @gradeName

				select @CourseNum = count(0),@HourNum = sum(ClassHour)
				from dbo.Grade 
				join dbo.Subject ON dbo.Grade.GradeID = dbo.Subject.GradeID
				where GradeName = @gradeName
			end
	go

	declare @gn varchar(5) = 'S2'
	declare @cn int
	declare @hn int

	exec usp_s4 @gn,@cn output,@hn output

	print '学期名称		课程总数	总课时'
	print @gn+'              '+cast(@cn as varchar(5))+'          '+cast(@hn as varchar(5))

--训练5 使用存储过程插入新课程记录
	--需求说明：
		--如果学期名称或者课程名称为空，则终止存储过程执行返回
		--如果课程所属学期不存在，要添加学期记录
		--返回增加的课程编号对应的学期编号
		--获得操作的结果，并根据不同的执行结果显示相应的提示信息

	if exists(select * from sysobjects where name='usp_s5')
		drop procedure usp_s5
	go

	create procedure usp_s5
	@gradeName varchar(5) = null,
	@subjectName varchar(20) = null
	as 
		declare @gradeId int
		declare @subjectId int
		
		if @gradeName is null or @subjectName is null
			begin
				raiserror('学期名或课程名不能为空',16,1)
				return
			end
		else
			begin
				--如果学期记录不存在,增加学期记录,并获取编号
				if not exists(select * from [dbo].[Grade] where [GradeName] = @gradeName)
					begin
						insert [dbo].[Grade] ([GradeName]) values (@gradeName)
						select @gradeId = GradeID from [dbo].[Grade] where [GradeName] = @gradeName
						print '增加学期记录成功 编号：'+cast(@gradeId as varchar(5))
					end

				--如果课程记录不存在,增加课程记录
				if not exists(select * from [dbo].[Subject] where [SubjectName] = @subjectName)
					begin
						insert into [dbo].[Subject] ([SubjectName],[ClassHour],[GradeID]) values (@subjectName,40,@gradeId)
						select @subjectId =[SubjectNo] from [dbo].[Subject] where [SubjectName] = @subjectName
						print '增加课程记录成功 编号：'+cast(@subjectId as varchar(5))
					end
			end
	go

	exec usp_s5 'S2','mvc'