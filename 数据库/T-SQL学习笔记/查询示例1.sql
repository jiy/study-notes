USE MySchool

DECLARE @No int				--学号
DECLARE @Name varchar(20)	--姓名
DECLARE @Age int			--年龄
DECLARE @Yeas int			--出生年份

SET @No=20011				--学号赋值

--查询姓名、年龄、出生年份并赋值
SELECT @Name=StudentName,@Age=FLOOR(DATEDIFF(DY,BornDate,GETDATE())/365),@Yeas=DATEPART(YYYY,BornDate) FROM Student WHERE StudentNo=@No

--显示姓名、年龄
PRINT '姓名：'+@Name+'  年龄:'+CAST(@Age AS varchar(20))+'岁'

--显示比该学生大一岁和小一岁的学生信息
SELECT * FROM Student WHERE DATEPART(YYYY,BornDate)=@Yeas+1 OR DATEPART(YYYY,BornDate)=@Yeas-1 
