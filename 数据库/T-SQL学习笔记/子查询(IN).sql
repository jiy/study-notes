USE MySchool
GO

--查询S1、S2开设的课程
SELECT sj.SubjectName FROM Subject sj WHERE										--根据编号查询开设的课程
sj.GradeID IN									
(SELECT g.GradeID FROM Grade g WHERE g.GradeName='S1' OR g.GradeName='S2')		--根据课程名称查到年级编号