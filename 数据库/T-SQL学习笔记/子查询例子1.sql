USE MySchool

GO

--查询Java Logic最近一次考试最高分和最低分
SELECT MAX(r.StudentResult) AS 'MAX', MIN(r.StudentResult) AS 'MIN' FROM Result r WHERE		--筛选最大值和最小值
 r.ExamDate=
	(SELECT MAX(r.ExamDate) FROM Result r WHERE												--找到该课程最近的考试时间
	r.SubjectNo=
		(SELECT sj.SubjectNo FROM Subject sj WHERE sj.SubjectName='Java Logic')				--找到课程对应的编号
	)
GO



