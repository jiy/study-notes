USE MySchool
GO

--如果有S1的学生，就将他在读年级更新为S2
IF(
	EXISTS(																			--如果有数据
		SELECT stu.StudentNo FROM Student stu WHERE									--根据年级id查找学生
		stu.GradeID=
			(SELECT g.GradeID FROM Grade g WHERE g.GradeName='S1')					--查询S1的id
		)
	)
BEGIN				--跟新学生年级
	 UPDATE Student 
	 SET GradeID=
		(SELECT g.GradeID FROM Grade g WHERE g.GradeName='S2') WHERE				--查询S2的id 
	 GradeID=
		(SELECT g.GradeID FROM Grade g WHERE g.GradeName='S1')						--查询S1的id
END