USE MySchool
GO

BEGIN TRANSACTION				--事务开始

DECLARE @err int				--定义变量统计错误

INSERT INTO [dbo].[Result] ([StudentNo],[SubjectNo],[ExamDate],[StudentResult]) VALUES (10000,2,'2015-07-17',10)
INSERT INTO [dbo].[Result] ([StudentNo],[SubjectNo],[ExamDate],[StudentResult]) VALUES (10001,2,'2015-07-17',65)
INSERT INTO [dbo].[Result] ([StudentNo],[SubjectNo],[ExamDate],[StudentResult]) VALUES (10002,2,'2015-07-17',32)
INSERT INTO [dbo].[Result] ([StudentNo],[SubjectNo],[ExamDate],[StudentResult]) VALUES (10011,2,'2015-07-17',102)	--违反约束，出现错误
INSERT INTO [dbo].[Result] ([StudentNo],[SubjectNo],[ExamDate],[StudentResult]) VALUES (10012,2,'2015-07-17',25)
INSERT INTO [dbo].[Result] ([StudentNo],[SubjectNo],[ExamDate],[StudentResult]) VALUES (20011,2,'2015-07-17',98)

SET @err=@err+@@ERROR			--统计错误

SELECT * FROM Result

IF(@err=0)						--如果没有错误
	BEGIN
		PRINT 'OK'
		COMMIT TRANSACTION		--提交事务
	END
ELSE
	BEGIN
		PRINT '回滚'
		PRINT @err
		ROLLBACK TRANSACTION	--回滚(撤销)事务
	END

SELECT * FROM Result

--delete from result  where ExamDate = '2015-07-17'