--删除数据库
USE master			--设置当前数据库为master,方便访问sysdatabases表
GO					--与后面语句区分开

--IF START
IF EXISTS (SELECT * FROM sysdatabases WHERE name = 'MySchool')	--判断数据库是否存在
	DROP DATABASE MySchool
GO
--END IF