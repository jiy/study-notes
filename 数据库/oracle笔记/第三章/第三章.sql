--上机练习1
  --1、根据公式：（工资-起征点）*对应税率-速算扣除数，计算员工KING所交税金
  declare 
    v_sal number(7,2);
    v_taxes number(7,2);
  begin
    select sal into v_sal from emp where ename = 'KING';
    dbms_output.put_line('员工的工资是: ' || v_sal);
    v_taxes:=(v_sal-3500)*3/100;
    dbms_output.put_line('应该交税: ' || v_taxes || '元');
  end;
  
  --2、根据员工SCOTT入职时间修改发放奖金列（comm列）。大于等于6年的奖金为2000元；小雨6年的奖金为1500元。
  declare
    v_hiredate date;
    v_bonus number(7,2);
  begin
    select hiredate into v_hiredate from emp where ename = 'SCOTT';
    
    if MONTHS_BETWEEN(sysdate, v_hiredate) /12 >= 6 then
      v_bonus := 2000;
    else
      v_bonus := 1500;
    end if;
    
    update emp set comm = v_bonus where ename = 'SCOTT';
  end;
  
  --3、根据表3-8（略）查询员工SCOTT相应的工资级别并显示所在部门名称、薪水和所在级别。
  
  declare
    v_sal number(7,2);
    v_rank number(2,0);
    v_dname varchar(20);
  begin
    select sal into v_sal from emp where ename = 'SCOTT';
    if v_sal >= 700 and v_sal <=3200 then
      v_rank:=1;
    elsif v_sal >= 3201 and v_sal <=4400 then
      v_rank:=2;
    elsif v_sal >= 4401 and v_sal <=5000 then
      v_rank:=3;
    elsif v_sal >= 5001 and v_sal <=7000 then
      v_rank:=4;
    elsif v_sal >= 7001 and v_sal <=9999 then
      v_rank:=5;
    end if;
    
    dbms_output.put_line('薪水：' || v_sal || '    所在级别：' || v_rank);
    
    select DNAME into v_dname from emp 
    join dept on dept.deptno = emp.deptno
    where ename = 'SCOTT';
    
    dbms_output.put_line('部门名称：' || v_dname);
    
  end;
  
  --4、为员工SCOTT增加工资，每次增加100元，直到10000元停止。
  declare
    v_sal number(7,2);
  begin
    select sal into v_sal from emp where ename = 'SCOTT';
    while v_sal< 10000 loop
      update emp set sal = sal+100 where ename = 'SCOTT';
      select sal into v_sal from emp where ename = 'SCOTT';
      dbms_output.put_line(v_sal);
    end loop;
  end;


--上机练习2
  --公司通过员工表(employee)维护职员记录。用以接收职员编号并检索职员姓名。姓名储存在v_ename变量中，类型为VARCHAR2(4)。
  declare
    v_ename varchar2(4);
  begin
    select ename into v_ename from emp where empno = 7839;
    
    dbms_output.put_line(v_ename);
    exception 
      when no_data_found then
        dbms_output.put_line('没有找到记录！');
        return;
      when value_error then
        dbms_output.put_line('员工姓名长度过长！');
  end;  

--上机练习3
  --要求编写一个程序，用以接收用户输入的员工编号、工资和部门代码。如果部门代码为“10”，且工资低于10000元，则更新员工的工资为10000，如果工资高于10000元，则显示消息“工资不低于10000元”。如果部门代码不为“10”则不显示。
  declare
    v_eno emp.empno%type;
    v_sal emp.sal%type;
    v_deptno emp.deptno%type;
  begin
    dbms_output.put_line('请输入部门编号');
    v_deptno := &部门编号;
    if v_deptno = '10' then
      dbms_output.put_line('请输入工资');
      v_sal := &员工工资;
      if v_sal >= 10000 then 
        dbms_output.put_line('工资高于10000元');
      else
        v_eno := &员工编号;
        update emp set sal = '10000' where empno = v_eno;
        dbms_output.put_line('工资已更新！');
      end if;
    end if;
  end;

--上机练习4
  --根据上机练习1完善如下需求
    --1、计算公司应缴税金的总额。
      declare
        v_taxes number(10,2) := 0;
        v_count number(10,2) := 0;
        cursor emp_cursor is select sal from emp;
      begin
        for emp_record in emp_cursor loop
          v_taxes := ((emp_record.sal-3500)*3/100);
          if v_taxes > 0 then
            v_count := v_count + v_taxes;
          end if;
        end loop;
        dbms_output.put_line(v_count);
      end;
      
    --2、根据员工入职时间修改所有员工发放奖金列(comm列)。大于等于6年的，奖金为2000元，小于6年的，奖金为1500元。
    declare
      cursor emp_cursor is select hiredate,empno from emp for update ;
    begin
      for emp_record in emp_cursor loop
        if  months_between(sysdate,emp_record.hiredate)/12 > 6 then
          update emp set comm = 2000 where current of emp_cursor ;
        else
          update emp set comm = 1500 where current of emp_cursor ;
        end if;  
      end loop;
    end;
    
    
    
    --3、根据表3-8（略）查询部门为“销售部”的员工的相应工资级别并显示员工姓名、所在部门名称、薪水和所在级别。
    --（为了节约时间，没有查询工资级别）
    declare
      cursor emp_cursor is select ename,sal from emp where deptno = (select deptno from dept where dname = 'SALES');
    begin
      for emp_record in emp_cursor loop
           dbms_output.put_line('姓名：' || emp_record.ename || '     工资：' || emp_record.sal);
      end loop;
    end;
