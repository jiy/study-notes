--上机练习1
  --创建tp_oeders表空间，大小为10MB，文件大小可自动扩展

  create tablespace tp_orders 
  datafile 'tp_orders.dbf'
  size 10 m
  autoextend on;

  --创建用户名为A_oe的用户到tp_orders中，密码为bdqn

  create user A_oe
  identified by bdqn
  default tablespace tp_orders;

  --授予A_oe用户connect、resource权限和访问A_hr用户的emploee表的权限

  grant connect,resource to A_oe;
  grant select on scott.emp to A_oe;
  
--上机练习2
    
  --创建一个从60开始、间隔为10、最大值是10000的序列对象dept_seq
  
  create sequence dept_seq
  start with 60
  increment by 10
  maxvalue 10000;
  
  --将序列值作为部门编号的值插入dept表
  insert into scott.dept (DEPTNO,DNAME,LOC) values(dept_seq.Nextval,'部门1','地址');
  insert into scott.dept (DEPTNO,DNAME,LOC) values(dept_seq.Nextval,'部门2','地址');
  insert into scott.dept (DEPTNO,DNAME,LOC) values(dept_seq.Nextval,'部门3','地址');
  
  --创建新表，命名为deptBak，将其作为模拟迁移后的dept表，并把迁移前的dept表的数据完全插入到deptBak中
  
  create table deptBak as select * from scott.dept;
  
  --重新创建序列dept_seq
  
  drop sequence dept_seq;
  
  create sequence dept_seq
  start with 600
  increment by 10
  maxvalue 10000;
  
  --使用序列插入数据到deptBak表
  insert into deptBak (DEPTNO,DNAME,LOC) values(dept_seq.Nextval,'部门4','地址');
  
--上机练习4
  --为了提高客户表（customers）数据检索效率，需要为该表创建索引。请针对客户编号、名、姓氏、地域列创建合适的索引。
         --建议步骤：
              --1、根据反向索引的特点可以为客户编号列创建反向键索引或者创建唯一索引。
              create index reverse_customers_CUSTOMER_ID on customers(CUSTOMER_ID) reverse;
              --2、根据位图索引的特点可以为地域列创建位图索引。
              create bitmap index bitmap_customers_NLS_TERRITORY on customers(NLS_TERRITORY);
              --3、根据组合索引的特点，可以为名和姓氏创建组合索引。
              
              
              
--上机练习5
  --使用A_oe用户登录，根据订单表orders创建范围分区表rangeOrders，插入‘2013/01/01’数据并查看，删除第三个分区内容并查看。
    --建议步骤:
      --1、用A_oe用户登录orcl数据库
        connect A_oe/bdqn;
      --2、根据Orders表创建范围分区rangeOrders。要求创建5个分区：
        --① part1分区为‘2005/01/01’前的数据
        --② part2分区为‘2006/01/01’前的数据
        --...
        --⑤ part5分区为‘2009/01/01’前的数据
          CREATE TABLE orders
          ( order_id           NUMBER(12)
          , order_date         DATE NOT NULL
          , order_mode         VARCHAR2(8)
          , customer_id        NUMBER(6) NOT NULL
          , order_status       NUMBER(2)
          , order_total        NUMBER(8,2)
          , sales_rep_id       NUMBER(6)
          , promotion_id       NUMBER(6)
          ) 
          partition by range(order_date)
          (
            partition p1 values less than (to_date('2005/01/01','yyyy/mm/dd')),
            partition p2 values less than (to_date('2006/01/01','yyyy/mm/dd')),
            partition p3 values less than (to_date('2007/01/01','yyyy/mm/dd')),
            partition p4 values less than (to_date('2008/01/01','yyyy/mm/dd')),
            partition p5 values less than (to_date('2009/01/01','yyyy/mm/dd'))
            --,partition p6 values less than (maxvalue)
          );
      --3、插入数据
      --4、查看第三分区数据
          select * from orders partition(p3);
      --5、删除第三分区数据
          delete from orders partition(p3);
              
